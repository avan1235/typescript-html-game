import * as DB from './db.js';
import * as Quiz from './quiz.js';
import * as Time from './time.js';
import * as Div from './divs.js';
import * as Util from './utils.js';

// CONSTANT DIV WITH ID

const STATS_WIDTH_CHANGE = 1200;
const COUNT_BEST_ON_BOARD = 10;

// GLOBAL VARIABLES

const QUIZ_MAP = new Map(Quiz.AVAILABLE_QUIZ.map(q => [q.id, q]));
let CURRENT_QUIZ_ID: number = null;
let MAX_QUESTION_ID: number = null;
let ANSWERS: number[] = null;
let COUNT_TIMES: Quiz.CountTime[] = null;
let STEP_BOX_LISTENERS: (() => void)[] = null; // to get it easily created, removed and restored during animations

// FUNCTIONS DEFINITIONS

function getStepBoxById(id: number): HTMLDivElement {
    return Util.queryDiv(`.stepBox[stepId=\'${id}\']`);
}

function getAllStepBox(): NodeListOf<HTMLDivElement> {
    return document.querySelectorAll('.stepBox');
}

function markStepBoxAsCurrent(stepId: number) {
    const curQuestion = getCurrentQuestion();
    const curId = getQuestionId(curQuestion);
    if (curId === stepId || stepId < 0 || stepId > MAX_QUESTION_ID) {
        return;
    }
    const curStepBox = Util.queryDiv('.stepBox[current]');
    const nextStepBox = getStepBoxById(stepId);

    curStepBox.removeAttribute('current');
    nextStepBox.setAttribute('current', '');
}

function setStepBoxMarked(stepId: number) {
    const stepBox = getStepBoxById(stepId);
    stepBox.setAttribute('marked', '');
}

function setStepBoxUnmarked(stepId: number) {
    const stepBox = getStepBoxById(stepId);
    stepBox.removeAttribute('marked');
}

function getCurrentQuestion(): HTMLDivElement {
    return Util.queryDiv('.singleQuestion[current]');
}

function getQuestionId(question: HTMLDivElement): number {
    return parseInt(question.getAttribute('questionId'), 10);
}

function getCurrentQuestionId(): number {
    return getQuestionId(getCurrentQuestion());
}

function getQuestionById(id: number): HTMLDivElement {
    return Util.queryDiv(`.singleQuestion[questionId=\'${id}\']`);
}

function isQuizGameFinished(): boolean {
    return ANSWERS.every(el => el !== null);
}

function manageFinishButtonVisibility(): void {
    if (isQuizGameFinished()) {
        Div.finish.removeAttribute('hidden');
    }
    else {
        Div.finish.setAttribute('hidden', '');
    }
}

function updateGameTime(timeValue: Time.TimeValue): void {
    Div.time.textContent = `Game time: ${Time.format(timeValue)}`;
}

function manageControlsVisibility(): void {
    const curQuestion = getCurrentQuestion();
    const curQuestionId = getQuestionId(curQuestion);

    if (curQuestionId === 0) {
        Div.prevButton.style.opacity = '0';
        Time.waitHalfSecond().then(() => {
            Div.prevButton.style.visibility = 'hidden';
        });
    }
    else {
        Div.prevButton.style.visibility = 'visible';
        Div.prevButton.style.opacity = '1';
    }

    if (curQuestionId === MAX_QUESTION_ID) {
        Div.nextButton.style.opacity = '0';
        Time.waitHalfSecond().then(() => {
            Div.nextButton.style.visibility = 'hidden';
        });
    }
    else {
        Div.nextButton.style.visibility = 'visible';
        Div.nextButton.style.opacity = '1';
    }
}

function updatePenaltyTime(penTime: number): void {
    Div.penalty.textContent = `Penalty time: ${penTime}s`;
}

function showQuestion(questionId: number): Promise<void> {
    const curQuestion = getCurrentQuestion();
    const curId = getQuestionId(curQuestion);
    if (curId === questionId || questionId < 0 || questionId > MAX_QUESTION_ID) {
        return;
    }
    const nextQuestion = getQuestionById(questionId);

    if (questionId > curId) {
        nextQuestion.style.transform = 'translate3d(100%, 0, 0)';
    }
    else {
        nextQuestion.style.transform = 'translate3d(-100%, 0, 0)';
    }
    markStepBoxAsCurrent(questionId);
    updatePenaltyTime(QUIZ_MAP.get(CURRENT_QUIZ_ID).questions[questionId].badAnswerPenaltyTime);
    Time.changeIncrementedTo(COUNT_TIMES[questionId]);

    // here is the moment where for a while in document are 2 'current' questions
    // just for animation purpose
    nextQuestion.setAttribute('current', '');

    if (questionId > curId) {
        curQuestion.style.transform = 'translate3d(-100%, 0, 0)';
    }
    else {
        curQuestion.style.transform = 'translate3d(100%, 0, 0)';
    }

    return Time.waitShort().then(() => {
        nextQuestion.style.transform = 'translate3d(0, 0, 0)';
     }).then(Time.waitHalfSecond).then(() => {
        curQuestion.removeAttribute('current');
        curQuestion.style.transform = 'translate3d(0, 0, 0)';
    }).then(manageControlsVisibility);
}

function showShiftedQuestion(shift: number): Promise<void> {
    const curQuestion = getCurrentQuestion();
    const shiftedId = getQuestionId(curQuestion) + shift;
    return showQuestion(shiftedId);
}

function showNextQuestion(): Promise<void> {
    return showShiftedQuestion(1);
}

function showPrevQuestion(): Promise<void> {
    return showShiftedQuestion(-1);
}

function focusCurrentQuestionInput(): void {
    const input = getCurrentQuestion().querySelector('.answerInputField');
    if (input !== null) {
        (input as HTMLInputElement).focus();
    }
}

function nextButtonListener(): void {
    removeControlsListeners();
    const curQuestionId = getQuestionId(getCurrentQuestion());
    if (curQuestionId !== MAX_QUESTION_ID) {
        showNextQuestion().then(addControlsListeners);
    }
    else {
        addControlsListeners();
    }
}

function prevButtonListener(): void {
    removeControlsListeners();
    const curQuestionId = getQuestionId(getCurrentQuestion());
    if (curQuestionId !== 0) {
        showPrevQuestion().then(addControlsListeners);
        document.addEventListener('keydown', keyboardListener);
    }
    else {
        addControlsListeners();
    }
}

function removeControlsListeners(): void {
    // disable clicks for animation time
    Div.nextButton.removeEventListener('click', nextButtonListener);
    Div.prevButton.removeEventListener('click', prevButtonListener);
    document.removeEventListener('keydown', keyboardListener);
}

function addControlsListeners(): void {
    Div.nextButton.addEventListener('click', nextButtonListener);
    Div.prevButton.addEventListener('click', prevButtonListener);
    document.addEventListener('keydown', keyboardListener);
}

function removeStepBoxListeners(): void {
    document.querySelectorAll('.stepBox').forEach(box => {
        const stepId = parseInt(box.getAttribute('stepId'), 10);
        box.removeEventListener('click', STEP_BOX_LISTENERS[stepId]);
    });
}

function addStepBoxListeners(): void {
    document.querySelectorAll('.stepBox').forEach(box => {
        const stepId = parseInt(box.getAttribute('stepId'), 10);
        box.addEventListener('click', STEP_BOX_LISTENERS[stepId]);
    });
}

function keyboardListener(event: KeyboardEvent): void {
    switch(event.code) {
        case 'ArrowRight': {
            nextButtonListener();
        } break;
        case 'ArrowLeft': {
            prevButtonListener();
        } break;
        case 'ArrowDown': {
            focusCurrentQuestionInput();
        } break;
        case 'Enter': {
            if (isQuizGameFinished()) {
                goToGameStats();
            }
        } break;
    }
}

function manageStepBoxOverflow(): void {
    if (Util.isWidthOverflown(Div.progress)) {
        Div.progress.setAttribute('overflown', '');
    }
    else {
        Div.progress.removeAttribute('overflow');
    }
}

function getQuestionAnswers(questionId: number): NodeListOf<HTMLDivElement> {
    return document.querySelectorAll(`.singleQuestion[questionId=\'${questionId}\'] .singleAnswer`);
}

function addSingleAnswersListeners(): void {
    document.querySelectorAll('.singleQuestion .singleAnswer').forEach(answer => {
        answer.addEventListener('click', () => {
            const question = answer.closest('.singleQuestion') as HTMLDivElement;
            const questionId = getQuestionId(question);
            const curAnswer = parseInt(answer.textContent, 10);
            if (ANSWERS[questionId] === null) {
                answer.setAttribute('selected', '');
                ANSWERS[questionId] = curAnswer;
                setStepBoxMarked(questionId);
            }
            else if (ANSWERS[questionId] === curAnswer) {
                answer.removeAttribute('selected');
                ANSWERS[questionId] = null;
                setStepBoxUnmarked(questionId);
            }
            else {
                getQuestionAnswers(questionId).forEach(singleAnswer => {
                    singleAnswer.removeAttribute('selected');
                })
                answer.setAttribute('selected', '');
                ANSWERS[questionId] = curAnswer;
                setStepBoxMarked(questionId);
            }
            manageFinishButtonVisibility();
        });
    });
}

function isValidNumber(input: string): boolean {
    return new RegExp('^(\-|(\-)?[1-9][0-9]*|0)?$').test(input);
}

function addInputListener(input: HTMLInputElement): void {
    const container = input.parentElement;
    const label = container.querySelector('.inputLabel');

    input.addEventListener('focusin', () => {
        label.setAttribute('focused', '');
        container.setAttribute('focused', '');
    });

    input.addEventListener('focusout', () => {
        if (input.value === '') {
            label.removeAttribute('focused');
        }
        container.removeAttribute('focused');
    });
}

function addInputFilteredListener(input: HTMLInputElement): void {
    addInputListener(input);

    ['input', 'keydown', 'keyup', 'mousedown', 'mouseup', 'select', 'contextmenu', 'drop'].forEach(event => {
        input.addEventListener(event, function () {
            if (isValidNumber(this.value)) {
                this.oldValue = this.value;
                this.oldSelectionStart = this.selectionStart;
                this.oldSelectionEnd = this.selectionEnd;
            }
            else if (this.hasOwnProperty('oldValue')) {
                this.value = this.oldValue;
                this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
            }
            else {
                this.value = '';
            }

            const inVal = this.value;
            const answer = (inVal === '' || inVal === '-') ? null : parseInt(inVal, 10);
            const question = input.closest('.singleQuestion') as HTMLDivElement;
            const questionId = getQuestionId(question);
            ANSWERS[questionId] = answer;
            if (answer == null) {
                setStepBoxUnmarked(questionId);
            }
            else {
                setStepBoxMarked(questionId);
            }
            manageFinishButtonVisibility();
        });
      });
}

function addInputAnswersListeners() {
    document.querySelectorAll('.singleQuestion .answerInputField').forEach(input => {
        addInputFilteredListener(input as HTMLInputElement);
    });
}

function toggleDescription(): void {
    if (!Div.sideContainer.hasAttribute('hidden')) {
        Div.sideContainer.setAttribute('hidden', '');
    }
    else {
        Div.sideContainer.removeAttribute('hidden');
    }
}

function createSingleQuestion(question: Quiz.Question, id: number): HTMLDivElement {
    const titleQ = Util.createDivWithClasses('questionTitle', question.title);
    const body = Util.createDivWithClasses('question', question.body);

    const answersQ = Util.createDivWithClasses('answersBody');
    if (question.possibleAnswers.length > 0) {
        for (const ans of question.possibleAnswers) {
            const single = Util.createDivWithClasses('singleAnswer clickable', `${ans}`);
            answersQ.appendChild(single);
        }
    }
    else {
        const inputContainer = Util.createDivWithClasses('inputContainer');
        const inputLabel = Util.createDivWithClasses('inputLabel', 'Your answer');
        inputContainer.appendChild(inputLabel);

        const answerInputField = document.createElement('input');
        answerInputField.classList.add('answerInputField');
        answerInputField.setAttribute('autocomplete', 'off');
        inputContainer.appendChild(answerInputField);

        answersQ.appendChild(inputContainer);
    }

    const questionBody = document.createElement('questionBody');
    questionBody.classList.add('questionBody');
    questionBody.appendChild(titleQ);
    questionBody.appendChild(body);
    questionBody.appendChild(answersQ);

    const questionBox = Util.createDivWithClasses('questionBox');
    questionBox.appendChild(questionBody);

    const singleQuestion = Util.createDivWithClasses('singleQuestion');
    singleQuestion.setAttribute('questionId', `${id}`);
    singleQuestion.appendChild(questionBox);

    if (id === 0) {
        singleQuestion.setAttribute('current', '');
    }

    return singleQuestion;
}

function buildQuizWeb(quizData: Quiz.Quiz): void {
    Div.title.textContent = quizData.title;
    Util.removeChildrenOf(Div.description);
    for (const par of quizData.description.split('\n')) {
        const p = document.createElement('p');
        p.textContent = par;
        Div.description.appendChild(p);
    }
    Util.removeChildrenOf(Div.progress);
    Util.removeChildrenOf(Div.questions);
    for (let i = 0; i < quizData.questions.length; i++) {
        const stepBox = Util.createDivWithClasses('stepBox clickable', `${i + 1}`);
        stepBox.setAttribute('stepId', `${i}`);
        if (i === 0) {
            stepBox.setAttribute('current', '');
        }
        Div.progress.appendChild(stepBox);
        Div.questions.appendChild(createSingleQuestion(quizData.questions[i], i));
    }

    MAX_QUESTION_ID = quizData.questions.length - 1;
    ANSWERS = Array.from({length: quizData.questions.length}, () => null);
    COUNT_TIMES = Array.from({length: quizData.questions.length}, () => {
        return { seconds: 0 }
    });

    // we keep them as the  global variable to get the ability to remove/add them
    STEP_BOX_LISTENERS = Array.from(getAllStepBox(), box => () => {
        removeStepBoxListeners(); // disable click for animation time
        const stepId = parseInt(box.getAttribute('stepId'), 10);
        const questionId = getCurrentQuestionId();
        if (questionId === stepId) {
            addStepBoxListeners();
        }
        else {
            showQuestion(stepId).then(addStepBoxListeners);
        }
    });

    updatePenaltyTime(QUIZ_MAP.get(CURRENT_QUIZ_ID).questions[0].badAnswerPenaltyTime);
    Time.changeIncrementedTo(COUNT_TIMES[0]);
    manageControlsVisibility();
    addControlsListeners();
    addStepBoxListeners();
    addSingleAnswersListeners();
    manageFinishButtonVisibility();
    addInputAnswersListeners();
    Div.sideContainer.setAttribute('hidden', '');
    Time.waitSecond().then(() => {
        Time.resetAndRun(updateGameTime);
        Time.waitSecond().then(Time.waitQuarterSecond).then(manageStepBoxOverflow);
    });
}

// HELLO SITE

function setupBegin(): void {
    Div.gameContainer.style.display = 'none';
    Div.gameContainer.style.opacity = '0.0';
    Div.gameContainer.style.transform = 'scale(0.5)';

    Div.resultsContainer.style.display = 'none';
    Div.shadow.setAttribute('hidden', '');
    Div.resultsBoard.setAttribute('hidden', '');

    Div.statsTitle.textContent = `Top ${COUNT_BEST_ON_BOARD} Results`;

    createQuizList();
}

function animateFlexContainers(from: HTMLDivElement, to: HTMLDivElement): void {
    from.style.transform = 'scale(0.5)';
    from.style.opacity = '0.0';
    Time.waitQuarterSecond().then(() => {
        from.style.display = 'none';
        to.style.display = 'flex';
    }).then(Time.waitQuarterSecond).then(() => {
        to.style.opacity = '1.0';
        to.style.transform = 'scale(1.0)';
    });
}

function animateFromSelectionToStats(): void {
    if (Util.getDeviceWidth() <= STATS_WIDTH_CHANGE) {
        animateFlexContainers(Div.selection, Div.stats);
    }
}

function animateFromStatsToSelection(): void {
    if (Util.getDeviceWidth() <= STATS_WIDTH_CHANGE) {
        animateFlexContainers(Div.stats, Div.selection);
        document.querySelectorAll('.singleQuizChoice').forEach(el => {
            el.removeAttribute('selected');
        });
    }
}

function animateFromQuizToSelection(): void {
    animateFlexContainers(Div.gameContainer, Div.helloContainer);
    animateFromStatsToSelection();
    Time.waitHalfSecond().then(setupBegin);
}

function animateFromSaveToBegin(): void {
    animateFromStatsToSelection();
    Div.shadow.setAttribute('hidden', '');
    Div.resultsBoard.setAttribute('hidden', '');
    Time.waitSecond().then(() => {
        animateFlexContainers(Div.resultsContainer, Div.helloContainer);
        Time.waitHalfSecond().then(() => {
            Div.resultsContainer.style.opacity = '1.0';
            Div.resultsContainer.style.transform = 'scale(1.0)';
        }).then(setupBegin);
    });
}

function runGameForSelectedQuiz(): void {
    CURRENT_QUIZ_ID = parseInt(Div.startButton.getAttribute('runQuizId'), 10);
    buildQuizWeb(QUIZ_MAP.get(CURRENT_QUIZ_ID));
    animateFlexContainers(Div.helloContainer, Div.gameContainer);
    document.addEventListener('keydown', keyboardListener);
}

function addQuizSelectionListeners(): void {
    const selectors = document.querySelectorAll('.singleQuizChoice');
    selectors.forEach(el => {
        el.addEventListener('click', () => {
            if (el.hasAttribute('selected')) {
                el.removeAttribute('selected');
                Div.statsBoard.setAttribute('hidden', '');
                Div.startButton.setAttribute('hidden', '');
            }
            else {
                const id = parseInt(el.getAttribute('quizId'), 10);
                Div.statsBoard.removeAttribute('hidden');
                Div.startButton.removeAttribute('hidden');
                Div.startButton.setAttribute('runQuizId', `${id}`);
                selectors.forEach(elem => { elem.removeAttribute('selected'); });
                el.setAttribute('selected', '');
                DB.selectBest(id, COUNT_BEST_ON_BOARD).then(createRanking);
                animateFromSelectionToStats();
            }
        });
    });
}

function addStatsDetailsListeners(): void {
    document.querySelectorAll('.statsPosition').forEach(el => {
        el.addEventListener('click', () => {
            const details = el.querySelector('.statsDetails') as HTMLDivElement;
            if (details.getAttribute('hidden') == null) {
                details.setAttribute('hidden', '');
            }
            else {
                details.removeAttribute('hidden');
            }
        });
    });
}

function addGameStats(results: boolean[], to: HTMLDivElement, penalties: number[] = null): void {
    results.forEach((valid, index) => {
        const answer = Util.createDivWithClasses('answerHistory', `${index + 1}`);
        if (valid) {
            answer.setAttribute('valid', 'true');
        }
        else {
            answer.setAttribute('valid', 'false');
        }
        if (penalties === null) {
            to.appendChild(answer);
        }
        else {
            const container = Util.createDivWithClasses('answerHistoryContainer');
            const penaltyQ = Util.createDivWithClasses('answerHistoryPenalty', `Penalty time: ${penalties[index]}s`);
            container.appendChild(answer);
            container.appendChild(penaltyQ);
            to.appendChild(container);
        }
    });
}

function getTimeFormatted(resultS: number): string {
    if (resultS >= 60) {
        return `${Math.floor(resultS / 60)}m ${resultS % 60}s`;
    }
    else {
        return `${resultS}s`;
    }
}

function addPositionList(resultQ: Quiz.Result, index: number): void {
    const position = Util.createDivWithClasses('statsPosition');
    if (resultQ.answersResults.length > 0) {
        position.classList.add('clickable');
    }

    const id = Util.createDivWithClasses('statsId', `${index}.`);

    const content = Util.createDivWithClasses('statsContent');
    const name = Util.createDivWithClasses('statsPointsName', `${resultQ.username} with time ${getTimeFormatted(resultQ.timeS)}`);
    const details = Util.createDivWithClasses('statsDetails');
    details.setAttribute('hidden', '');

    addGameStats(resultQ.answersResults, details);

    content.appendChild(name);
    content.appendChild(details);

    position.appendChild(id);
    position.appendChild(content);

    Div.positions.appendChild(position);
}

function addQuizListEntry(quizData: Quiz.Quiz, index: number): void {
    const choice = Util.createDivWithClasses('singleQuizChoice clickable');
    choice.setAttribute('quizId', `${quizData.id}`)
    const id = Util.createDivWithClasses('quizId', `${index}`);
    const name = Util.createDivWithClasses('quizName', quizData.title);

    choice.appendChild(id);
    choice.appendChild(name);
    Div.scrollQuiz.appendChild(choice);
}

function createRanking(results: Quiz.Result[]): void {
    Util.removeChildrenOf(Div.positions);
    results.forEach((resultQ, index) => {
        addPositionList(resultQ, index + 1);
    });
    addStatsDetailsListeners();
}

function createQuizList(): void {
    Div.statsBoard.setAttribute('hidden', '');
    Div.startButton.setAttribute('hidden', '');
    Util.removeChildrenOf(Div.scrollQuiz);
    Quiz.AVAILABLE_QUIZ.forEach((quizData, index) => {
        addQuizListEntry(quizData, index + 1);
    });
    CURRENT_QUIZ_ID = -1;
    addQuizSelectionListeners();
}

// RESULT BOARD

function getInputUserName(): string {
    const name = Div.nameInput.value.trim();
    const validated = name.length > 0 ? name : 'Anonymous';
    return validated;
}

function saveAndAnimateToBegin(id: number, timeValue: number, results: boolean[], questionsTimes: number[]): void {
    const resultQ = Quiz.getResultFor(id, timeValue, results, questionsTimes, getInputUserName());
    DB.insert(resultQ);
    animateFromSaveToBegin();
}

function setupResultsBoard(): void {
    const currentTime = Time.stop();
    const playTime = currentTime.minutes * 60 + currentTime.seconds;
    const id = CURRENT_QUIZ_ID;
    const quizData = QUIZ_MAP.get(id);
    const goodAnswers = Array.from({length: quizData.questions.length}, (_, i) => quizData.questions[i].correctAnswer);
    const results = ANSWERS.map((ans, index) => ans === goodAnswers[index]);
    const addTime = quizData.questions.map((q, index) => results[index] ? 0 : q.badAnswerPenaltyTime)
                                  .reduce((acc, v) => acc + v, 0);
    const questionsTimes = COUNT_TIMES.map(t => t.seconds);

    const resultTime = playTime + addTime;
    Div.result.textContent = getTimeFormatted(resultTime);
    Util.removeChildrenOf(Div.gameStats);
    addGameStats(results, Div.gameStats, quizData.questions.map((q, index) => results[index] ? 0 : q.badAnswerPenaltyTime));

    // these nodes needs recreation to remove listeners on them
    const noStats = Util.queryDiv('#saveWithoutStats');
    const withStats = Util.queryDiv('#saveWithStats');

    [noStats, withStats].forEach(Util.recreateNode);
    Util.queryDiv('#saveWithoutStats').addEventListener('click', () => saveAndAnimateToBegin(id, resultTime, [], []));
    Util.queryDiv('#saveWithStats').addEventListener('click', () => saveAndAnimateToBegin(id, resultTime, results, questionsTimes));
}

function goToGameStats(): void {
    document.removeEventListener('keydown', keyboardListener);
    setupResultsBoard();
    Div.resultsContainer.style.display = 'flex';
    Time.waitShort().then(() => {
        Div.shadow.removeAttribute('hidden');
        Div.resultsBoard.removeAttribute('hidden');
    });
}

// STATS GENERATION

function generateRandomStats(): void {
    const quizStats = ['{"quizId":1,"username":"Weronika","answersResults":[],"answersTimes":[9,8,25,15],"timeS":61}', '{"quizId":1,"username":"Patryk","answersResults":[],"answersTimes":[5,15,25,18],"timeS":69}', '{"quizId":1,"username":"Felicjan","answersResults":[],"answersTimes":[16,18,26,6],"timeS":71}', '{"quizId":1,"username":"Zenon","answersResults":[],"answersTimes":[21,23,21,7],"timeS":74}', '{"quizId":1,"username":"Krzysztof","answersResults":[],"answersTimes":[25,13,7,28],"timeS":82}', '{"quizId":1,"username":"Remek","answersResults":[false,false,false,false],"answersTimes":[6,27,24,21],"timeS":85}', '{"quizId":1,"username":"Anna","answersResults":[],"answersTimes":[10,19,18,21],"timeS":91}', '{"quizId":1,"username":"Łukasz","answersResults":[],"answersTimes":[29,11,12,20],"timeS":97}', '{"quizId":1,"username":"Tomasz","answersResults":[true,true,false,true],"answersTimes":[25,16,29,7],"timeS":106}', '{"quizId":1,"username":"Joanna","answersResults":[false,true,true,true],"answersTimes":[18,22,19,28],"timeS":112}', '{"quizId":2,"username":"Zenon","answersResults":[],"answersTimes":[12,6,24,28,8,26,5,6,22,25,16,20,6,14,5,27,15,30,6,29,5,15,30,17,17],"timeS":416}', '{"quizId":2,"username":"Joanna","answersResults":[false,false,false,false,true,false,false,true,false,false,false,true,true,true,false,true,true,false,false,true,true,false,false,true,false],"answersTimes":[7,13,22,8,5,20,21,24,16,29,11,21,20,23,15,13,10,16,20,24,25,8,6,24,11],"timeS":417}', '{"quizId":2,"username":"Patryk","answersResults":[],"answersTimes":[11,13,20,19,7,7,6,30,15,13,27,28,30,29,25,14,14,18,7,10,15,20,12,16,17],"timeS":428}', '{"quizId":2,"username":"Anna","answersResults":[],"answersTimes":[20,6,20,24,20,15,21,11,24,15,26,6,21,27,11,16,9,17,16,13,8,28,23,8,24],"timeS":439}', '{"quizId":2,"username":"Tomasz","answersResults":[false,true,false,true,false,false,true,true,true,true,true,false,true,false,true,false,true,false,false,false,true,false,true,false,true],"answersTimes":[26,23,6,29,19,18,16,7,7,22,17,23,14,16,23,22,17,16,28,14,19,24,8,19,16],"timeS":455}', '{"quizId":2,"username":"Krzysztof","answersResults":[true,true,true,false,false,false,true,false,false,false,false,false,false,true,false,true,true,true,false,false,false,false,true,false,false],"answersTimes":[8,26,13,13,14,12,26,30,6,8,21,14,18,16,26,29,22,17,6,7,13,17,20,24,22],"timeS":458}', '{"quizId":2,"username":"Łukasz","answersResults":[true,false,false,true,true,true,false,false,true,true,false,false,true,true,false,false,false,true,false,true,false,false,false,false,false],"answersTimes":[16,7,10,14,12,19,30,10,25,28,28,27,24,24,5,23,26,14,28,8,19,7,10,20,26],"timeS":470}', '{"quizId":2,"username":"Weronika","answersResults":[false,true,false,true,false,false,true,true,false,true,true,true,true,false,false,true,false,false,false,true,false,true,false,false,true],"answersTimes":[7,16,23,28,30,8,26,11,29,18,13,25,5,20,7,9,27,22,29,26,26,19,18,6,14],"timeS":478}', '{"quizId":2,"username":"Remek","answersResults":[],"answersTimes":[10,25,30,19,14,25,23,20,7,29,15,25,5,22,27,23,28,14,16,19,26,20,23,14,8],"timeS":493}', '{"quizId":2,"username":"Felicjan","answersResults":[],"answersTimes":[17,26,29,19,20,8,7,16,5,28,27,28,24,7,6,9,23,28,12,27,15,29,13,22,29],"timeS":497}', '{"quizId":3,"username":"Joanna","answersResults":[false,true,true,false,false,false,true,true,false],"answersTimes":[5,26,27,7,13,19,7,7,19],"timeS":146}', '{"quizId":3,"username":"Łukasz","answersResults":[true,false,true,true,false,false,false,true,false],"answersTimes":[6,6,10,21,27,19,20,16,19],"timeS":152}', '{"quizId":3,"username":"Krzysztof","answersResults":[],"answersTimes":[6,21,12,6,20,24,17,13,18],"timeS":155}', '{"quizId":3,"username":"Patryk","answersResults":[false,true,false,false,true,false,true,false,true],"answersTimes":[30,14,9,22,24,16,16,10,12],"timeS":159}', '{"quizId":3,"username":"Remek","answersResults":[true,false,true,false,false,false,true,true,false],"answersTimes":[7,16,19,23,20,7,30,6,18],"timeS":168}', '{"quizId":3,"username":"Zenon","answersResults":[false,false,false,false,false,false,false,false,true],"answersTimes":[17,21,20,11,26,18,7,15,21],"timeS":172}', '{"quizId":3,"username":"Tomasz","answersResults":[true,true,false,false,true,false,true,false,true],"answersTimes":[16,29,9,22,19,24,19,15,21],"timeS":197}', '{"quizId":3,"username":"Anna","answersResults":[false,false,false,false,false,true,false,false,false],"answersTimes":[6,27,28,16,24,21,27,21,29],"timeS":199}', '{"quizId":3,"username":"Weronika","answersResults":[true,true,false,false,false,false,true,false,true],"answersTimes":[13,27,28,25,24,6,24,15,29],"timeS":200}', '{"quizId":3,"username":"Felicjan","answersResults":[false,true,true,false,false,false,true,true,false],"answersTimes":[20,24,7,27,24,29,30,10,30],"timeS":207}'];
    quizStats.map(el => JSON.parse(el) as Quiz.Result).forEach(DB.insert);
    alert('Generating stats data for quizes.');
}

// SETUP

Div.generateButton.addEventListener('click', generateRandomStats);
Div.menuButton.addEventListener('click', toggleDescription);
Div.mainBackButton.addEventListener('click', animateFromStatsToSelection);
Div.startButton.addEventListener('click', runGameForSelectedQuiz);
Div.finishButton.addEventListener('click', goToGameStats);
Div.noSave.addEventListener('click', animateFromSaveToBegin);
Div.closeButton.addEventListener('click', animateFromQuizToSelection);
addInputListener(Div.nameInput);

setupBegin();
