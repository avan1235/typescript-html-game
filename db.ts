import { Result } from './quiz.js'

const dbName = 'db-algebra-quiz';
const storeName = 'quiz-results';

export function isSupported(): boolean {
    return 'indexedDB' in window;
}

function createQuizStore(db: IDBDatabase): void {
    const objectStore = db.createObjectStore(storeName, { autoIncrement: true });
    objectStore.createIndex('quizIdTimeS', ['quizId', 'timeS'], { unique: false });
}

function putResultToDatabase(db: IDBDatabase, data: Result): void {
    const transaction = db.transaction(storeName, 'readwrite');
    const store = transaction.objectStore(storeName);
    store.put(data);
}

export function insert(data: Result): Promise<boolean> {
    const request = indexedDB.open(dbName);

    return new Promise((resolve, _reject) => {
        request.onsuccess = (e: any) => {
            const db = e.target.result;

            if (!db.objectStoreNames.contains(storeName)) {
                db.close();
                resolve(false);
            }
            else {
                putResultToDatabase(db, data);
                db.close();
                resolve(true);
            }
        };
        request.onerror = (_e: any) => {
            resolve(false);
        };
        request.onupgradeneeded = (e: any) => {
            const db = e.target.result;
            createQuizStore(db);
        };
    });
}

export  function selectBest(quizId: number, count: number): Promise<Result[]> {
    try {
        const request = indexedDB.open(dbName);

        return new Promise((resolve, reject) => {
            request.onsuccess = (e: any) => {
                const db = e.target.result;

                const result = Array<Result>();
                if (!db.objectStoreNames.contains(storeName)) {
                    resolve(result); // return empty data store if not exists
                }
                else {
                    const transaction = db.transaction(storeName, 'readonly');
                    const store = transaction.objectStore(storeName);

                    const index = store.index('quizIdTimeS');
                    const cursorRequest = index.openCursor(IDBKeyRange.bound([quizId, 0], [quizId + 1, 0], false, true));
                    let i = 0;

                    cursorRequest.onsuccess = elem => {
                        const cursor = elem.target.result;
                        if (cursor && i < count) {
                            result.push(cursor.value);
                            i += 1;
                            cursor.continue();
                        }
                        else {
                            db.close();
                            resolve(result);
                        }
                    };
                    cursorRequest.onerror = elem => {
                        db.close();
                        reject(elem.target.error);
                    };
                }
            };
            request.onerror = (e: any) => {
                reject(e.target.error);
            };
            request.onupgradeneeded = (e: any) => {
                const db = e.target.result;
                createQuizStore(db);
            };
        });
    } catch(error) {
        return new Promise((_resolve, reject) => { reject(error) });
    }
}

