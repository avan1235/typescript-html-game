export interface Question {
    title: string;
    body: string;
    possibleAnswers: number[];
    correctAnswer: number;
    badAnswerPenaltyTime: number;
}

export interface Quiz {
    title: string;
    id: number;
    description: string;
    questions: Question[];
}

export interface CountTime {
    seconds: number;
}

export interface Result {
    quizId: number;
    username: string;
    timeS: number;
    answersResults: boolean[];
    answersTimes: number[];
}

export function getResultFor(id: number, timeValue: number, results: boolean[], questionsTimes: number[], user: string): Result {
    return  {
        quizId: id,
        username: user,
        timeS: timeValue,
        answersResults: results,
        answersTimes: questionsTimes
    };
}

function isValidString(data: any): boolean {
    return ('string' === typeof data && data.length > 0);
}

function isIntegerNumber(data: any): boolean {
    return ('number' === typeof data && Number.isInteger(data));
}

function isNotEmptyArray(data: any): boolean {
    return (Array.isArray(data) && data.length > 0);
}

function isValidQuestion(jsonData: any): boolean {
    if ('object' !== typeof jsonData) {
        return false;
    }
    let hasTitle = false;
    let hasBody = false;
    let hasAnswer = false;
    let hasPossibleAnswers = false;

    for (const property in jsonData) {
        if (property === 'title') {
            if (isValidString(jsonData[property])) {
                hasTitle = true;
            }
            else {
                return false;
            }
        }
        else if (property === 'body') {
            if (isValidString(jsonData[property])) {
                hasBody = true;
            }
            else {
                return false;
            }
        }
        else if (property === 'correctAnswer') {
            if (isIntegerNumber(jsonData[property])) {
                hasAnswer = true;
            }
            else {
                return false;
            }
        }
        else if (property === 'possibleAnswers') {
            if (Array.isArray(jsonData[property])) {
                if (jsonData[property].length > 0) {
                    hasPossibleAnswers = true;
                    const answers = new Set();
                    for (const answer of jsonData[property]) {
                        if (!isIntegerNumber(answer) || answers.has(answer)) {
                            return false;
                        }
                        answers.add(answer);
                    }
                }
            }
            else {
                return false;
            }
        }
    }

    if (!hasAnswer || !hasTitle || !hasBody) {
        return false;
    }

    if (hasPossibleAnswers) {
        let isCorrectAnswerInPossible = false;
        for (const answer of jsonData.possibleAnswers) {
            if (answer === jsonData.correctAnswer) {
                isCorrectAnswerInPossible = true;
            }
        }
        return isCorrectAnswerInPossible;
    }
    return true;
}

function isValidQuiz(jsonData: any): boolean {
    if ('object' !== typeof jsonData) {
        return false;
    }
    let hasTitle = false;
    let hasDescription = false;
    let hasQuestions = false;
    let hasId = false;

    for (const property in jsonData) {
        if (property === 'title') {
            if (isValidString(jsonData[property])) {
                hasTitle = true;
            }
            else {
                return false;
            }
        }
        else if (property === 'description') {
            if (isValidString(jsonData[property])) {
                hasDescription = true;
            }
            else {
                return false;
            }
        }
        else if (property === 'id') {
            if (isIntegerNumber(jsonData[property])) {
                hasId = true;
            }
            else {
                return false;
            }
        }
        else if (property === 'questions') {
            if (isNotEmptyArray(jsonData[property])) {
                hasQuestions = true;
                for (const question of jsonData[property]) {
                    if (!isValidQuestion(question)) {
                        hasQuestions = false; // one of questions is invalid
                    }
                }
            }
            else {
                return false;
            }
        }
    }

    return hasTitle && hasDescription && hasQuestions;
}

const availableQuizData = [
    '{"title":"Simple Quiz","id":1,"description":"Some description of this quiz could be there but in my opinion it\u0027s not important to create some unused data that will not get read by anybody.\\nThat\u0027s why my decision is to leave this space blank and don\u0027t allow other people spend time on reading it.\\nSome description of this quiz could be there but in my opinion it\u0027s not important to create some unused data that will not get read by anybody.\\nThat\u0027s why my decision is to leave this space blank and don\u0027t allow other people spend time on reading it.\\nSome description of this quiz could be there but in my opinion it\u0027s not important to create some unused data that will not get read by anybody.\\nThat\u0027s why my decision is to leave this space blank and don\u0027t allow other people spend time on reading it.\\nSome description of this quiz could be there but in my opinion it\u0027s not important to create some unused data that will not get read by anybody.\\nThat\u0027s why my decision is to leave this space blank and don\u0027t allow other people spend time on reading it.\\nSome description of this quiz could be there but in my opinion it\u0027s not important to create some unused data that will not get read by anybody.\\nThat\u0027s why my decision is to leave this space blank and don\u0027t allow other people spend time on reading it.\\nSome description of this quiz could be there but in my opinion it\u0027s not important to create some unused data that will not get read by anybody.\\nThat\u0027s why my decision is to leave this space blank and don\u0027t allow other people spend time on reading it.\\nSome description of this quiz could be there but in my opinion it\u0027s not important to create some unused data that will not get read by anybody.\\nThat\u0027s why my decision is to leave this space blank and don\u0027t allow other people spend time on reading it.\\nSome description of this quiz could be there but in my opinion it\u0027s not important to create some unused data that will not get read by anybody.\\nThat\u0027s why my decision is to leave this space blank and don\u0027t allow other people spend time on reading it.\\nSome description of this quiz could be there but in my opinion it\u0027s not important to create some unused data that will not get read by anybody.\\nThat\u0027s why my decision is to leave this space blank and don\u0027t allow other people spend time on reading it.\\nSome description of this quiz could be there but in my opinion it\u0027s not important to create some unused data that will not get read by anybody.\\nThat\u0027s why my decision is to leave this space blank and don\u0027t allow other people spend time on reading it.\\nSome description of this quiz could be there but in my opinion it\u0027s not important to create some unused data that will not get read by anybody.\\nThat\u0027s why my decision is to leave this space blank and don\u0027t allow other people spend time on reading it.\\nSome description of this quiz could be there but in my opinion it\u0027s not important to create some unused data that will not get read by anybody.\\nThat\u0027s why my decision is to leave this space blank and don\u0027t allow other people spend time on reading it.\\nSome description of this quiz could be there but in my opinion it\u0027s not important to create some unused data that will not get read by anybody.\\nThat\u0027s why my decision is to leave this space blank and don\u0027t allow other people spend time on reading it.\\nSome description of this quiz could be there but in my opinion it\u0027s not important to create some unused data that will not get read by anybody.\\nThat\u0027s why my decision is to leave this space blank and don\u0027t allow other people spend time on reading it.\\nSome description of this quiz could be there but in my opinion it\u0027s not important to create some unused data that will not get read by anybody.\\nThat\u0027s why my decision is to leave this space blank and don\u0027t allow other people spend time on reading it.\\nSome description of this quiz could be there but in my opinion it\u0027s not important to create some unused data that will not get read by anybody.\\nThat\u0027s why my decision is to leave this space blank and don\u0027t allow other people spend time on reading it.","questions":[{"title":"Questions 1.","body":"What\u0027s the number covered by ## in\\n16 06 68 88 ## 98?","possibleAnswers":[],"correctAnswer":87,"badAnswerPenaltyTime":10},{"title":"Questions 2.","body":"If:\\n    2 + 3 \u003d 8\\n    3 + 7 \u003d 27\\n    4 + 5 \u003d 32\\n    5 + 8 \u003d 60\\n    6 + 7 \u003d 72\\nThen:\\n    7 + 8 \u003d ?","possibleAnswers":[],"correctAnswer":98,"badAnswerPenaltyTime":10},{"title":"Questions 3.","body":"What\u0027s the answer for\\n7 + 7 ÷ 7 + 7 x 7 - 7 \u003d ?","possibleAnswers":[],"correctAnswer":50,"badAnswerPenaltyTime":10},{"title":"Questions 4.","body":"If:\\n    1 + 4 \u003d 5\\n    2 + 5 \u003d 12\\n    3 + 6 \u003d 21\\nThen:\\n    8 + 11 \u003d ?","possibleAnswers":[19,91,96,50],"correctAnswer":96,"badAnswerPenaltyTime":10}]}',
    '{"title":"Real Math Quiz","id":2,"description":"This is the hardest quiz in the world.\\nThere are many tricky match questions that you can fight with.\\nIn our opinion you should try to solve it at least once in your life.","questions":[{"title":"Question 1.","body":"What\u0027s the answer for\\n    -719 - 348 \u003d ?","possibleAnswers":[],"correctAnswer":-1067,"badAnswerPenaltyTime":14},{"title":"Question 2.","body":"What\u0027s the answer for\\n    -25 + 737 \u003d ?","possibleAnswers":[712,710,705,708],"correctAnswer":712,"badAnswerPenaltyTime":13},{"title":"Question 3.","body":"What\u0027s the answer for\\n    397 ÷ 657 \u003d ?","possibleAnswers":[],"correctAnswer":0,"badAnswerPenaltyTime":10},{"title":"Question 4.","body":"What\u0027s the answer for\\n    -775 + -2 \u003d ?","possibleAnswers":[-777,-776,-786,-772],"correctAnswer":-777,"badAnswerPenaltyTime":19},{"title":"Question 5.","body":"What\u0027s the answer for\\n    141 ÷ 68 \u003d ?","possibleAnswers":[],"correctAnswer":2,"badAnswerPenaltyTime":12},{"title":"Question 6.","body":"What\u0027s the answer for\\n    460 - 744 \u003d ?","possibleAnswers":[],"correctAnswer":-284,"badAnswerPenaltyTime":18},{"title":"Question 7.","body":"What\u0027s the answer for\\n    703 ÷ -94 \u003d ?","possibleAnswers":[],"correctAnswer":-7,"badAnswerPenaltyTime":16},{"title":"Question 8.","body":"What\u0027s the answer for\\n    691 + -853 \u003d ?","possibleAnswers":[-162,-165,-157,-164],"correctAnswer":-162,"badAnswerPenaltyTime":13},{"title":"Question 9.","body":"What\u0027s the answer for\\n    232 ÷ -862 \u003d ?","possibleAnswers":[],"correctAnswer":0,"badAnswerPenaltyTime":20},{"title":"Question 10.","body":"What\u0027s the answer for\\n    -474 x 547 \u003d ?","possibleAnswers":[],"correctAnswer":-259278,"badAnswerPenaltyTime":10},{"title":"Question 11.","body":"What\u0027s the answer for\\n    235 + 189 \u003d ?","possibleAnswers":[424,434,429,427],"correctAnswer":424,"badAnswerPenaltyTime":12},{"title":"Question 12.","body":"What\u0027s the answer for\\n    -674 - 618 \u003d ?","possibleAnswers":[-1292,-1298,-1284,-1302],"correctAnswer":-1292,"badAnswerPenaltyTime":16},{"title":"Question 13.","body":"What\u0027s the answer for\\n    -54 ÷ 32 \u003d ?","possibleAnswers":[],"correctAnswer":-1,"badAnswerPenaltyTime":13},{"title":"Question 14.","body":"What\u0027s the answer for\\n    -991 ÷ 113 \u003d ?","possibleAnswers":[],"correctAnswer":-8,"badAnswerPenaltyTime":14},{"title":"Question 15.","body":"What\u0027s the answer for\\n    947 - 419 \u003d ?","possibleAnswers":[528,524,521,535],"correctAnswer":528,"badAnswerPenaltyTime":12},{"title":"Question 16.","body":"What\u0027s the answer for\\n    998 + 86 \u003d ?","possibleAnswers":[],"correctAnswer":1084,"badAnswerPenaltyTime":19},{"title":"Question 17.","body":"What\u0027s the answer for\\n    -788 - 20 \u003d ?","possibleAnswers":[],"correctAnswer":-808,"badAnswerPenaltyTime":12},{"title":"Question 18.","body":"What\u0027s the answer for\\n    143 x -709 \u003d ?","possibleAnswers":[-101387,-101380,-101388,-101397],"correctAnswer":-101387,"badAnswerPenaltyTime":11},{"title":"Question 19.","body":"What\u0027s the answer for\\n    -721 x 620 \u003d ?","possibleAnswers":[],"correctAnswer":-447020,"badAnswerPenaltyTime":20},{"title":"Question 20.","body":"What\u0027s the answer for\\n    -800 ÷ 871 \u003d ?","possibleAnswers":[0,-8,4,-9],"correctAnswer":0,"badAnswerPenaltyTime":15},{"title":"Question 21.","body":"What\u0027s the answer for\\n    961 + -868 \u003d ?","possibleAnswers":[],"correctAnswer":93,"badAnswerPenaltyTime":17},{"title":"Question 22.","body":"What\u0027s the answer for\\n    -429 - -475 \u003d ?","possibleAnswers":[46,44,40,48],"correctAnswer":46,"badAnswerPenaltyTime":10},{"title":"Question 23.","body":"What\u0027s the answer for\\n    -288 + -271 \u003d ?","possibleAnswers":[-559,-552,-564,-567],"correctAnswer":-559,"badAnswerPenaltyTime":13},{"title":"Question 24.","body":"What\u0027s the answer for\\n    694 + 500 \u003d ?","possibleAnswers":[],"correctAnswer":1194,"badAnswerPenaltyTime":18},{"title":"Question 25.","body":"What\u0027s the answer for\\n    786 - -692 \u003d ?","possibleAnswers":[1478,1470,1483,1484],"correctAnswer":1478,"badAnswerPenaltyTime":17}]}',
    '{"title":"Funny Random Quiz","id":3,"description":"This quiz is for fat and furious.\\nJust do it.\\nIt\u0027s simpler that it looks like.\\nMath is the magic.\\nYou can do it.\\nDon\u0027t forget about your life.\\nUniverse is a part of it.","questions":[{"title":"Question 1.","body":"What\u0027s the answer to the Ultimate Question of Life, the Universe, and Everything?","possibleAnswers":[],"correctAnswer":42,"badAnswerPenaltyTime":10},{"title":"Question 2.","body":"If:\\n    4 + x \u003d 46\\nThen:\\n    x \u003d ?","possibleAnswers":[],"correctAnswer":42,"badAnswerPenaltyTime":10},{"title":"Question 3.","body":"What\u0027s the sum of the first 6 positive even numbers?","possibleAnswers":[],"correctAnswer":42,"badAnswerPenaltyTime":10},{"title":"Question 4.","body":"What\u0027s the third primary pseudoperfect number?","possibleAnswers":[],"correctAnswer":42,"badAnswerPenaltyTime":10},{"title":"Question 5.","body":"What\u0027s the number of isomorphism classes of all simple and oriented directed graphs on 4 vertices?","possibleAnswers":[],"correctAnswer":42,"badAnswerPenaltyTime":10},{"title":"Question 6.","body":"What\u0027s the largest number n such that there exist positive integers p, q, r with\\n    1 \u003d 1/n + 1/p + 1/q + 1/r?","possibleAnswers":[],"correctAnswer":42,"badAnswerPenaltyTime":10},{"title":"Question 7.","body":"What\u0027s the smallest number k such that for every Riemann surface C\\n    #Aut(C) ≤ k deg(KC) \u003d k(2g − 2)","possibleAnswers":[],"correctAnswer":42,"badAnswerPenaltyTime":10},{"title":"Question 8.","body":"What\u0027s the only known value that is the number of sets of four distinct positive integers a, b, c, d, each less than the value itself, such that ab − cd, ac − bd, and ad − bc are each multiples of the value?","possibleAnswers":[],"correctAnswer":42,"badAnswerPenaltyTime":10},{"title":"Question 9.","body":"What\u0027s the third pentadecagonal number?","possibleAnswers":[],"correctAnswer":42,"badAnswerPenaltyTime":10}]}'
];
export const AVAILABLE_QUIZ = availableQuizData.map(q => JSON.parse(q))
                                          .filter(isValidQuiz)
                                          .map(q => q as Quiz);