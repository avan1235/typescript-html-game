export function removeChildrenOf(el: HTMLDivElement): void {
    while (el.firstChild) {
        el.removeChild(el.lastChild);
    }
}

export function createDiv(): HTMLDivElement {
    return document.createElement('div');
}

export function createDivWithClasses(classesNames: string = null, textContent: string = null): HTMLDivElement {
    const divCreated = createDiv();
    if (classesNames !== null) {
        classesNames.split(' ').forEach(name => {
            divCreated.classList.add(name);
        });
    }
    if (textContent !== null) {
        divCreated.textContent = textContent;
    }
    return divCreated;
}

export function queryDiv(name: string): HTMLDivElement {
    return document.querySelector(name);
}

export function getDeviceWidth(): number {
    return document.documentElement.clientWidth;
}

export function isWidthOverflown(elem: HTMLDivElement): boolean {
    return elem.scrollWidth > elem.clientWidth;
}

export function recreateNode(elem: HTMLDivElement): void {
    elem.parentNode.replaceChild(elem.cloneNode(true), elem);
}